#!/usr/bin/env bash

# Script to automatically create an appropriate commit for a submodule based on commit messages of submodule since
# last submodule bump.
# Basically creates a message like "submodule1: My most recent change #1 (and N other commits)\nThe other commits"
# Includes stripping of module path and shorthand 'p' for profile, specifically for use within Inuits.

# Add this file to your $PATH as 'git-bump' so git can automatically
# find it and add it to its arsenal of commands.

# ensure an argument is given
if [ -z "$1" ]; then
	echo "Submodule not specified"
	exit 1
fi

CHANGELOG="$(git submodule summary "$1")"
N_CHANGELOG_LINES=$(echo "$CHANGELOG" | wc -l)

# strip modules/{internal,upstream}/ and shorthand profile_ to p_
PREFIX="$(echo "$1" | sed 's.modules/\(internal\|upstream\)/..;s/profile_/p_/'): "

# get the first change and clean it up
FIRST_CHANGE_LINE="$(echo "$CHANGELOG" | grep '> ' | head -n 1 | sed 's/[ >]\+//')"

# determine a suffix to indicate more info in details
if ((N_CHANGELOG_LINES > 2)); then
	SUFFIX=" (+ $((N_CHANGELOG_LINES - 2)) other commits)"
else
	SUFFIX=''
fi

# build the commit message
CM=$(echo -e "${PREFIX}${FIRST_CHANGE_LINE}${SUFFIX}\n\n${CHANGELOG}")

# stage submodule changes
git add $1

# commit changes with previously built commit message
git commit -m "$CM"
